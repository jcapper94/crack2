#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.

int file_length(char *filename)
{

    struct stat info;
    int ret = stat(filename, &info);
    if(ret == -1)
    {
        return -1;
    }
    else
    {
        return info.st_size;
    }

}


int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    
    char *guessmd5 = md5(guess, strlen(guess));
    
    if (strcmp(hash, guessmd5) == 0)
    {
        free(guessmd5); // Free malloc after each instance return 1 or return 0.
        return 1;
    }
        free(guessmd5);
        return 0;

}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    char *p = malloc(len);
    
    FILE *f = fopen(filename,"r");
    if(!f)
    {
        printf(" Can't open %s for reading.\n", filename);
        exit(1);
    }

    fread(p, sizeof(char), len, f);
    
    int num = 0;
    
    for(int i = 0; i < len; i++)
    {
        if(p[i] == '\n')
        {
            p[i] = '\0';
            num++;
        }
    }
    
    
    // allocate space for array pointers
    
    char **ps = malloc(num * sizeof(char *));

    //load address to array
    
    ps[0] = &p[0];
    int j = 1;
    for(int i = 0; i < len-1; i++)
    {
        if (p[i] == '\0')
        {
            ps[j]= &p[i+1];
            j++;
        }

    }
    *size = num;
    fclose(f);
    return ps;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    
    int dlen = 0;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    
    FILE *fp = fopen(argv[1],"r");
    if (!fp)
    {
        printf("Can't open %s for reading.\n", argv[1]);
        exit(1);
    }

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    // int count = 0;
    
    int cracks = 0;
    char hash[HASH_LEN];
    while(fgets(hash, HASH_LEN, fp) != NULL)
    {
        // Replace new lines with nulls.
        
        if(hash[strlen(hash)-1] == '\n')
        {
            hash[strlen(hash)-1] = '\0';
        }
        for (int i = 0; i < dlen; i++)
        {
            // Parse through matches until one is found and then break.
            if (tryguess(hash, dict[i]) == 1)
            {
                printf("%s: %s\n", dict[i], hash);
                cracks++;
                break;
            }
        }
    }
    printf("%d passwords cracked.\n", cracks);
    fclose(fp);
}